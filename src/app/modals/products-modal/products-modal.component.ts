import {Component, OnInit} from '@angular/core';
import {WorkWithImages} from "../../shared/classes/work-with-images";

@Component({
    selector: 'app-products-modal',
    templateUrl: './products-modal.component.html',
    styleUrls: ['./products-modal.component.css']
})
export class ProductsModalComponent implements OnInit {
    bgResult: string;

    constructor() {
    }

    ngOnInit() {

    }

    upLoadBackground(readerEvt, iconOrBg) {
        WorkWithImages.upLoadBackground((result, iconOrBg) => this.setPicture(result, iconOrBg), readerEvt, iconOrBg);
    }

    setPicture(result, iconOrBg) {
        this[iconOrBg] = result;
    }

}
